package hu.mito.androidtest.presentation.dashboard.newitem.model

import androidx.databinding.ObservableField
import hu.mito.androidtest.domain.interactor.TodoItemInteractor
import hu.mito.androidtest.presentation.core.BaseViewModel
import hu.mito.androidtest.presentation.core.binding.util.command
import hu.mito.androidtest.presentation.core.binding.util.wrap
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.processors.PublishProcessor

class NewItemViewModel(
    private val itemInteractor: TodoItemInteractor
) : BaseViewModel() {

    val nameInput = ObservableField("")
    val quantityInput = ObservableField("")

    private val navigateToMainScreenProcessor = PublishProcessor.create<Any>()
    private val errorEventProcessor = PublishProcessor.create<Any>()

    val sendCommand = command {
        itemInteractor.addItem(
            nameInput.get() ?: "",
            quantityInput.get()?.toIntOrNull() ?: 0
        ).observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                errorEventProcessor.offer(Any())
            }
            .subscribe {
                navigateToMainScreenProcessor.offer(Any())
            }
    }

    fun getNavigateToMainScreenFlowable() = navigateToMainScreenProcessor.wrap()

}