package hu.mito.androidtest.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hu.mito.androidtest.R

class RootActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
    }
}