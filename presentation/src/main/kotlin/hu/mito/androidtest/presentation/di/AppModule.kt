package hu.mito.androidtest.presentation.di

import androidx.room.Room
import hu.mito.androidtest.database.AppDatabase
import org.koin.dsl.module

val appModule = module {

    single { Room.databaseBuilder(get(), AppDatabase::class.java, "test-db")
        .build() }
    single { get<AppDatabase>().tokenDao() }
    single { get<AppDatabase>().todoItemDao() }
}