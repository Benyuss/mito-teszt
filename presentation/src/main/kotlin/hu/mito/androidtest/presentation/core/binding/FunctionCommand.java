package hu.mito.androidtest.presentation.core.binding;


import java.util.concurrent.Callable;

import io.reactivex.functions.Function;
import timber.log.Timber;

public class FunctionCommand<T> extends ObservableCommand {
    private final Function<T, Runnable> command;
    private final Callable<Boolean> canExecuteCommand;

    public FunctionCommand(Function<T, Runnable> command) {
        this(command, () -> true);
    }

    public FunctionCommand(Function<T, Runnable> command, Callable<Boolean> canExecute) {
        this.command = command;
        this.canExecuteCommand = canExecute;
    }

    @Override
    public void execute() {
        execute(null);
    }

    public void execute(T arg) {
        try {
            command.apply(arg).run();

            if (arg != null) {
                executedEventsProcessor.onNext(arg);
            }
        } catch (Exception e) {
            Timber.v(e);
        }
    }

    @Override
    public boolean canExecute() {
        try {
            return canExecuteCommand != null && canExecuteCommand.call();
        } catch (Exception e) {
            Timber.v(e);
            return false;
        }
    }

    @Override
    public void canExecuteChanged() {
        notifyChange();
    }
}
