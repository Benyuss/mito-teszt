package hu.mito.androidtest.presentation.core.binding.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public interface ItemActionProvider<T> {

    void setOnItemClickedListener(@Nullable OnItemClickedListener<T> onItemClickedListener);

    void setOnItemLongClickedListener(@Nullable OnItemLongClickedListener<T> onItemLongClickedListener);

    interface OnItemClickedListener<T> {
        void onItemClicked(@NonNull T item);
    }

    interface OnItemLongClickedListener<T> {
        void onItemLongClicked(@NonNull T item);
    }
}
