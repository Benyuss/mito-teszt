package hu.mito.androidtest.presentation.core.binding.util;

import androidx.annotation.IntDef;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * A collection of factories to create RecyclerView LayoutManagers so that you can easily set them
 * in your layout. Adapted from: https://github.com/evant/binding-collection-adapter/blob/master/
 * bindingcollectionadapter-recyclerview/src/main/java/me/tatarka/bindingcollectionadapter2/LayoutManagers.java
 */
public class LayoutManagers {
    protected LayoutManagers() {
    }

    /**
     * A {@link LinearLayoutManager}.
     */
    public static LayoutManagerFactory linear() {
        return recyclerView -> new LinearLayoutManager(recyclerView.getContext());
    }

    /**
     * A {@link LinearLayoutManager} with the given orientation and reverseLayout.
     */
    public static LayoutManagerFactory linear(@Orientation final int orientation, final boolean reverseLayout) {
        return recyclerView -> new LinearLayoutManager(recyclerView.getContext(), orientation, reverseLayout);
    }

    /**
     * A {@link LinearLayoutManager} with LinearLayoutManager.VERTICAL orientation and without reverse
     */
    public static LayoutManagerFactory linearVertical() {
        return recyclerView -> new LinearLayoutManager(recyclerView.getContext(),
                RecyclerView.VERTICAL, false);
    }

    /**
     * A {@link LinearLayoutManager} with LinearLayoutManager.HORIZONTAL orientation and without reverse
     */
    public static LayoutManagerFactory linearHorizontal() {
        return recyclerView -> new LinearLayoutManager(recyclerView.getContext(),
                RecyclerView.HORIZONTAL, false);
    }

    /**
     * A {@link GridLayoutManager} with the given spanCount.
     */
    public static LayoutManagerFactory grid(final int spanCount) {
        return recyclerView -> new GridLayoutManager(recyclerView.getContext(), spanCount);
    }

    /**
     * A {@link GridLayoutManager} with the given spanCount, orientation and reverseLayout.
     **/
    public static LayoutManagerFactory grid(final int spanCount, @Orientation final int orientation,
                                            final boolean reverseLayout) {
        return recyclerView -> new GridLayoutManager(recyclerView.getContext(), spanCount, orientation, reverseLayout);
    }

    /**
     * A {@link StaggeredGridLayoutManager} with the given spanCount and orientation.
     */
    public static LayoutManagerFactory staggeredGrid(final int spanCount, @Orientation final int orientation) {
        return recyclerView -> new StaggeredGridLayoutManager(spanCount, orientation);
    }

    public interface LayoutManagerFactory {
        RecyclerView.LayoutManager create(RecyclerView recyclerView);
    }

    @IntDef({RecyclerView.HORIZONTAL, RecyclerView.VERTICAL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Orientation {
    }
}
