package hu.mito.androidtest.presentation.core.binding;

import java.util.concurrent.Callable;

import timber.log.Timber;

public class RunnableCommand extends ObservableCommand {
    private final Runnable command;
    private final Callable<Boolean> canExecuteCommand;

    public RunnableCommand(Runnable command) {
        this(command, () -> true);
    }

    public RunnableCommand(Runnable command, Callable<Boolean> canExecute) {
        this.command = command;
        this.canExecuteCommand = canExecute;
    }

    @Override
    public void execute() {
        command.run();
        executedEventsProcessor.onNext(new Object());
    }

    @Override
    public boolean canExecute() {
        try {
            return canExecuteCommand != null && canExecuteCommand.call();
        } catch (Exception e) {
            Timber.v(e);
            return false;
        }
    }

    @Override
    public void canExecuteChanged() {
        notifyChange();
    }
}
