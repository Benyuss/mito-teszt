package hu.mito.androidtest.presentation.dashboard.newitem.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import hu.mito.androidtest.R
import hu.mito.androidtest.databinding.FragmentNewItemBinding
import hu.mito.androidtest.presentation.dashboard.newitem.model.NewItemViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.viewmodel.ext.android.viewModel

class NewItemFragment : Fragment() {

    val viewModel: NewItemViewModel by viewModel()

    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_new_item, container, false)
        val binding = FragmentNewItemBinding.bind(view)
        binding.vm = viewModel
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getNavigateToMainScreenFlowable()
            .subscribe {
                requireActivity().onBackPressed()
            }.apply { disposables.add(this) }
    }

    override fun onDestroyView() {
        disposables.dispose()
        super.onDestroyView()
    }
}