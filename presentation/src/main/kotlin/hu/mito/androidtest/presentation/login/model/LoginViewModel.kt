package hu.mito.androidtest.presentation.login.model

import androidx.databinding.ObservableField
import hu.mito.androidtest.domain.interactor.LoginInteractor
import hu.mito.androidtest.presentation.core.BaseViewModel
import hu.mito.androidtest.presentation.core.binding.util.command
import hu.mito.androidtest.presentation.core.binding.util.wrap
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers

class LoginViewModel(
    private val loginInteractor: LoginInteractor
) : BaseViewModel() {

    val emailInput = ObservableField("")
    val passwordInput = ObservableField("")

    private val navigateToMainScreenProcessor = PublishProcessor.create<Any>()
    private val errorEventProcessor = PublishProcessor.create<Any>()

    val loginCommand = command {
        loginInteractor.login(emailInput.get() ?: "", passwordInput.get() ?: "")
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                errorEventProcessor.offer(Any())
            }
            .subscribe {
                navigateToMainScreenProcessor.offer(Any())
            }
    }

    fun getNavigateToMainScreenFlowable() = navigateToMainScreenProcessor.wrap()
    fun getErrorEvents() = errorEventProcessor.wrap()

}
