package hu.mito.androidtest.presentation.core.binding.util

import io.reactivex.Flowable
import io.reactivex.processors.FlowableProcessor

fun <T : Any> FlowableProcessor<T>.wrap(): Flowable<T> = FlowableUtil.wrapInFlowable(this)
