package hu.mito.androidtest.presentation.di

import hu.mito.androidtest.data.datastore.cloud.TokenCloudDataStore
import hu.mito.androidtest.data.datastore.local.TokenLocalDataStore
import hu.mito.androidtest.data.repository.TokenRepositoryImpl
import hu.mito.androidtest.domain.interactor.LoginInteractor
import hu.mito.androidtest.domain.repository.TokenRepository
import hu.mito.androidtest.network.controller.LoginController
import hu.mito.androidtest.presentation.login.model.LoginViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {

    factory { LoginController() }

    single { TokenCloudDataStore(get()) }
    single { TokenLocalDataStore(get()) }
    single<TokenRepository> { TokenRepositoryImpl(get(), get()) }
    single { LoginInteractor(get()) }

    viewModel { LoginViewModel(get()) }
}