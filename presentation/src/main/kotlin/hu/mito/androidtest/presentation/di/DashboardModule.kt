package hu.mito.androidtest.presentation.di

import hu.mito.androidtest.data.datastore.cloud.ItemCloudDataStore
import hu.mito.androidtest.data.datastore.local.TodoItemLocalDataStore
import hu.mito.androidtest.data.repository.TodoItemRepositoryImpl
import hu.mito.androidtest.domain.interactor.TodoItemInteractor
import hu.mito.androidtest.domain.repository.TodoItemRepository
import hu.mito.androidtest.network.controller.ItemController
import hu.mito.androidtest.presentation.dashboard.model.DashboardViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dashboardModule = module {

    factory { ItemController(get()) }
    single { ItemCloudDataStore(get())}
    single { TodoItemLocalDataStore(get()) }
    single<TodoItemRepository> { TodoItemRepositoryImpl(get(), get()) }
    single { TodoItemInteractor(get()) }
    viewModel { DashboardViewModel(get()) }
}