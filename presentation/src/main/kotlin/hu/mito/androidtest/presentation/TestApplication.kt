package hu.mito.androidtest.presentation

import android.app.Application
import hu.mito.androidtest.presentation.di.appModule
import hu.mito.androidtest.presentation.di.dashboardModule
import hu.mito.androidtest.presentation.di.loginModule
import hu.mito.androidtest.presentation.di.newItemModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@TestApplication)
            modules(arrayListOf(appModule, loginModule, dashboardModule, newItemModule))
        }
    }
}