package hu.mito.androidtest.presentation.core.binding.util

import hu.mito.androidtest.presentation.core.binding.Command
import hu.mito.androidtest.presentation.core.binding.FunctionCommand
import hu.mito.androidtest.presentation.core.binding.ObservableCommand
import hu.mito.androidtest.presentation.core.binding.RunnableCommand
import io.reactivex.Completable

fun Completable.toCommand() = toCommand { true }

fun Completable.toCommand(canExecute: () -> Boolean) = command(canExecute) { subscribe() }

fun Command.toCompletable() = Completable.fromAction {
    if (canExecute()) {
        execute()
    }
}

fun command(canExecute: () -> Boolean = { true }, action: () -> Unit) = RunnableCommand(action, canExecute)
        as ObservableCommand

inline fun <T : Any> command(noinline canExecute: () -> Boolean = { true }, crossinline action: (T) -> Unit) =
        FunctionCommand<T>({ Runnable { action.invoke(it) } }, canExecute)
