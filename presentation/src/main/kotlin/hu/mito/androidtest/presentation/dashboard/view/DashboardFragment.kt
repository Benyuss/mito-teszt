package hu.mito.androidtest.presentation.dashboard.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import hu.mito.androidtest.R
import hu.mito.androidtest.databinding.FragmentDashboardBinding
import hu.mito.androidtest.domain.model.TodoItem
import hu.mito.androidtest.presentation.core.Navigator
import hu.mito.androidtest.presentation.dashboard.model.DashboardViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_dashboard.itemList
import org.koin.android.viewmodel.ext.android.viewModel

class DashboardFragment : Fragment() {

    val viewModel : DashboardViewModel by viewModel()
    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val binding = FragmentDashboardBinding.bind(view)
        binding.vm = viewModel
        binding.itemList.adapter = DashboardItemAdapter()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getNavigateToAddItemEvents()
            .subscribe {
                Navigation.findNavController(requireActivity(), R.id.dashboardHost)
                    .navigate(DashboardFragmentDirections.actionDashboardFragmentToNewItemFragment())
            }.apply { disposables.add(this) }

        itemList.adapter?.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                if (itemCount == 1) {
                    itemList?.smoothScrollToPosition(positionStart)
                }
            }
        })
    }

    override fun onDestroyView() {
        disposables.dispose()
        super.onDestroyView()
    }
}