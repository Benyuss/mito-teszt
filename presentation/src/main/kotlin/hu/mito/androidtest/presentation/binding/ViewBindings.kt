package hu.mito.androidtest.presentation.binding

import android.view.View
import androidx.databinding.BindingAdapter
import hu.mito.androidtest.presentation.core.binding.Command

@BindingAdapter("command")
fun View.bindCommand(command: Command?) {
    command?.let {
        this.setOnClickListener {
            command.execute()
        }
    }
}