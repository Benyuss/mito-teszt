package hu.mito.androidtest.presentation.dashboard.model

import androidx.lifecycle.ViewModel
import hu.mito.androidtest.domain.interactor.TodoItemInteractor
import hu.mito.androidtest.presentation.core.binding.ReadOnlyField
import hu.mito.androidtest.presentation.core.binding.util.command
import hu.mito.androidtest.presentation.core.binding.util.wrap
import io.reactivex.processors.PublishProcessor

class DashboardViewModel(interactor: TodoItemInteractor) : ViewModel() {

    val items = ReadOnlyField.create(
        interactor.getItems()
            .map { items ->
                items.map { item -> DashboardItemViewModel(item) }
            }
    )

    private val navigateToAddItemProcessor = PublishProcessor.create<Any>()

    val addCommand = command {
        navigateToAddItemProcessor.offer(Any())
    }

    fun getNavigateToAddItemEvents() = navigateToAddItemProcessor.wrap()
}