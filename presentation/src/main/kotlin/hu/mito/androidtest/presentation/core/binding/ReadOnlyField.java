package hu.mito.androidtest.presentation.core.binding;

import androidx.annotation.NonNull;

import io.reactivex.Flowable;

public final class ReadOnlyField<T> extends RxObservableField<T> {
    private ReadOnlyField(@NonNull Flowable<T> source) {
        super(source);
    }

    @NonNull
    public static <U> ReadOnlyField<U> create(@NonNull Flowable<U> source) {
        return new ReadOnlyField<>(source);
    }

    @Override
    public void set(T value) {
        throw new UnsupportedOperationException();
    }
}
