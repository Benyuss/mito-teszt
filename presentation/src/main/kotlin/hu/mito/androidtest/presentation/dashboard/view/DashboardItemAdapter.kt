package hu.mito.androidtest.presentation.dashboard.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import hu.mito.androidtest.R
import hu.mito.androidtest.presentation.core.binding.util.BindableRecyclerViewAdapter
import hu.mito.androidtest.presentation.core.binding.util.SimpleDiffCallback
import hu.mito.androidtest.presentation.dashboard.model.DashboardItemViewModel

class DashboardItemAdapter : BindableRecyclerViewAdapter<DashboardItemViewModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(getViewDataBinding(parent, R.layout.item_todo))

    private fun getViewDataBinding(parent: ViewGroup, @LayoutRes layoutRes: Int): ViewDataBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            layoutRes,
            parent,
            false
        )

    override fun getItemId(position: Int) = getItem(position).item.hashCode().toLong()

    override fun getItemViewModel(item: DashboardItemViewModel) = item

    override fun getDiffCallback(): DiffUtil.ItemCallback<DashboardItemViewModel> =
        FontStyleItemViewModelCallback()

    private class FontStyleItemViewModelCallback :
        SimpleDiffCallback<DashboardItemViewModel>() {
        override fun areItemsTheSame(
            oldItem: DashboardItemViewModel,
            newItem: DashboardItemViewModel
        ) =
            oldItem.item == newItem.item
    }
}