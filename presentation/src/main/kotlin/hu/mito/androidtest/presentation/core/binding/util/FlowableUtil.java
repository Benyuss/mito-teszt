package hu.mito.androidtest.presentation.core.binding.util;

import android.content.SharedPreferences;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.processors.FlowableProcessor;

public final class FlowableUtil {

    private FlowableUtil() {
        // utility class
    }

    /**
     * Wraps the {@link FlowableProcessor} object into a {@link Flowable}, which
     * emits the same sequence as the original processor.
     * <br/>
     * Although {@link FlowableProcessor} already extends {@link Flowable}, it is
     * not possible to properly use {@link Flowable#share()} on it.
     *
     * @param processor the object to be wrapped
     * @param <T> the item value type
     * @return the processor wrapped in a {@link Flowable}
     */
    public static <T> Flowable<T> wrapInFlowable(FlowableProcessor<T> processor) {
        return Flowable.create(source -> {
            Disposable disposable = processor
                    .subscribe(source::onNext, source::onError);

            source.setCancellable(disposable::dispose);
        }, BackpressureStrategy.BUFFER);
    }
}
