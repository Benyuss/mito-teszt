package hu.mito.androidtest.presentation.core.binding;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public class RxObservableField<T> extends ObservableField<T> {
    private final Flowable<T> source;
    private final Map<OnPropertyChangedCallback, Disposable> disposables = new HashMap<>();

    RxObservableField(@NonNull Flowable<T> source) {
        this.source = source
                .doOnNext(RxObservableField.super::set)
                .doOnError(e -> Timber.e(e, "onError in source observable"))
                .onErrorResumeNext(Flowable.empty())
                .share();
    }

    @NonNull
    public static <U> RxObservableField<U> create(@NonNull Flowable<U> source) {
        return new RxObservableField<>(source);
    }

    @Override
    public synchronized void addOnPropertyChangedCallback(@NonNull OnPropertyChangedCallback callback) {
        super.addOnPropertyChangedCallback(callback);
        disposables.put(callback, source.subscribe());
    }

    @Override
    public synchronized void removeOnPropertyChangedCallback(@NonNull OnPropertyChangedCallback callback) {
        super.removeOnPropertyChangedCallback(callback);

        Disposable disposable = disposables.remove(callback);

        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
