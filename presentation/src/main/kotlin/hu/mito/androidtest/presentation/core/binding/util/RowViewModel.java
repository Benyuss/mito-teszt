package hu.mito.androidtest.presentation.core.binding.util;

import androidx.lifecycle.ViewModel;

public abstract class RowViewModel<T> extends ViewModel {
    private final T item;

    protected RowViewModel(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }
}
