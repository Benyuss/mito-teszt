package hu.mito.androidtest.presentation.binding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.mito.androidtest.presentation.core.binding.util.BindableRecyclerViewAdapter
import hu.mito.androidtest.presentation.core.binding.util.LayoutManagers

@BindingAdapter(value = ["items", "layoutManager"], requireAll = false)
fun <T> RecyclerView.bindItems(
    items: List<T>?,
    layoutManagerFactory: LayoutManagers.LayoutManagerFactory?
) {

    layoutManagerFactory?.let {
        layoutManager = layoutManagerFactory.create(this)
    }

    items?.let {
        val adapter = this.adapter as BindableRecyclerViewAdapter<T>?

        adapter?.swapItems(items)
    }
}