package hu.mito.androidtest.presentation.core.binding;

import androidx.databinding.BaseObservable;

import io.reactivex.Flowable;
import io.reactivex.processors.PublishProcessor;

public abstract class ObservableCommand extends BaseObservable implements Command {
    protected final PublishProcessor<Object> executedEventsProcessor = PublishProcessor.create();

    @Override
    public Flowable<Object> getExecutedEvents() {
        return executedEventsProcessor;
    }
}
