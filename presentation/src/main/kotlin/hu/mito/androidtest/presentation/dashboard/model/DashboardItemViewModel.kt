package hu.mito.androidtest.presentation.dashboard.model

import hu.mito.androidtest.domain.model.TodoItem
import hu.mito.androidtest.presentation.core.binding.util.RowViewModel

class DashboardItemViewModel(item: TodoItem) : RowViewModel<TodoItem>(item)