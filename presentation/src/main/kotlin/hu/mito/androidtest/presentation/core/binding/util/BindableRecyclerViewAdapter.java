package hu.mito.androidtest.presentation.core.binding.util;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.AdapterListUpdateCallback;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class BindableRecyclerViewAdapter<T>
        extends RecyclerView.Adapter<BindableRecyclerViewAdapter.ViewHolder>
        implements ItemActionProvider<T> {

    private final AsyncListDiffer<T> asyncListDiffer;

    private OnItemClickedListener<T> itemClickedListener;
    private OnItemLongClickedListener<T> itemLongClickedListener;

    protected BindableRecyclerViewAdapter() {
        asyncListDiffer = new AsyncListDiffer<>(new AdapterListUpdateCallback(this),
                new AsyncDifferConfig.Builder<>(getDiffCallback()).build());

        setHasStableIds(true);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final T item = getItem(position);

        holder.itemView.setOnClickListener(view -> {
            if (itemClickedListener != null) {
                itemClickedListener.onItemClicked(item);
            }
        });

        holder.itemView.setOnLongClickListener(view -> {
            if (itemLongClickedListener != null) {
                itemLongClickedListener.onItemLongClicked(item);
                return true;
            }

            return false;
        });

        ViewDataBinding binding = holder.getBinding();
        binding.setVariable(BR.vm, getItemViewModel(item));
        binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return asyncListDiffer.getCurrentList().size();
    }

    @NonNull
    protected T getItem(int position) {
        return asyncListDiffer.getCurrentList().get(position);
    }

    protected List<T> getItems() {
        return asyncListDiffer.getCurrentList();
    }

    @CallSuper
    public void swapItems(List<T> newItems) {
        asyncListDiffer.submitList(newItems);
    }

    @Override
    public void setOnItemClickedListener(@Nullable OnItemClickedListener<T> onItemClickedListener) {
        this.itemClickedListener = onItemClickedListener;
    }

    @Override
    public void setOnItemLongClickedListener(@Nullable OnItemLongClickedListener<T> onItemLongClickedListener) {
        this.itemLongClickedListener = onItemLongClickedListener;
    }

    @Override
    public abstract long getItemId(int position);

    @NonNull
    protected abstract ViewModel getItemViewModel(@NonNull T item);

    @NonNull
    protected abstract DiffUtil.ItemCallback<T> getDiffCallback();

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        public ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
            this.binding.executePendingBindings();
        }

        @NonNull
        public ViewDataBinding getBinding() {
            return binding;
        }
    }
}
