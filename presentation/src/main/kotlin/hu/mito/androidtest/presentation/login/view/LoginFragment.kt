package hu.mito.androidtest.presentation.login.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import hu.mito.androidtest.R
import hu.mito.androidtest.databinding.FragmentLoginBinding
import hu.mito.androidtest.presentation.login.model.LoginViewModel
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    val viewModel: LoginViewModel by viewModel()

    private val disposables = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        val binding = FragmentLoginBinding.bind(view)
        binding.vm = viewModel
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getErrorEvents()
            .subscribe {
                Toast.makeText(requireContext(), R.string.login_error, Toast.LENGTH_SHORT).show()
            }.apply { disposables.add(this) }

        viewModel.getNavigateToMainScreenFlowable()
            .subscribe {
                Navigation.findNavController(requireActivity(), R.id.dashboardHost)
                    .navigate(LoginFragmentDirections.actionLoginFragmentToDashboardFragment())
            }.apply { disposables.add(this) }
    }

    override fun onDestroyView() {
        disposables.dispose()
        super.onDestroyView()
    }
}