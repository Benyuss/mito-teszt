package hu.mito.androidtest.presentation.core.binding

import io.reactivex.Flowable

interface Command {

    val executedEvents: Flowable<Any>
    fun execute()

    fun canExecute(): Boolean

    fun canExecuteChanged()
}
