package hu.mito.androidtest.presentation.di

import hu.mito.androidtest.presentation.dashboard.newitem.model.NewItemViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newItemModule = module {

    viewModel { NewItemViewModel(get()) }
}