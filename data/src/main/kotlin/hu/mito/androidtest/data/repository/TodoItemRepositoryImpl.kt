package hu.mito.androidtest.data.repository

import hu.mito.androidtest.data.datastore.cloud.ItemCloudDataStore
import hu.mito.androidtest.data.datastore.local.TodoItemLocalDataStore
import hu.mito.androidtest.data.mapper.ItemEntityMapper
import hu.mito.androidtest.domain.model.TodoItem
import hu.mito.androidtest.domain.repository.TodoItemRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

class TodoItemRepositoryImpl(
    private val cloudDataStore: ItemCloudDataStore,
    private val localDataStore: TodoItemLocalDataStore
) : TodoItemRepository {

    override fun getAll(): Flowable<List<TodoItem>> =
        cloudDataStore.getAll()
            .observeOn(Schedulers.computation())
            .doOnNext {
                localDataStore.addItems(ItemEntityMapper.mapToList(it))
            }.onErrorResumeNext(localDataStore.getAll()
                .map { ItemEntityMapper.mapFromList(it) })

    override fun addItem(name: String, quantity: Int): Completable {
        return cloudDataStore.addItem(name, quantity)
    }
}