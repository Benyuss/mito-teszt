package hu.mito.androidtest.data.datastore.cloud

import hu.mito.androidtest.data.datastore.parent.ItemDataStore
import hu.mito.androidtest.data.mapper.ItemResponseMapper
import hu.mito.androidtest.domain.model.TodoItem
import hu.mito.androidtest.network.controller.ItemController
import hu.mito.androidtest.network.endpoint.ItemRequest
import io.reactivex.Completable
import io.reactivex.Flowable

class ItemCloudDataStore(private val controller: ItemController) : ItemDataStore {

    override fun getAll(): Flowable<List<TodoItem>> {
        return controller.getItems()
            .map { ItemResponseMapper.mapFromList(it) }
            .toFlowable()
    }

    fun addItem(name: String, quantity: Int): Completable {
        return controller.addItem(ItemRequest(name, quantity))
    }
}