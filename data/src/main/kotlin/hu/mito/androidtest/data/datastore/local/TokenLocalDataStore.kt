package hu.mito.androidtest.data.datastore.local

import hu.mito.androidtest.database.dao.TokenDao
import hu.mito.androidtest.database.entity.TokenEntity
import hu.mito.androidtest.domain.model.Token

class TokenLocalDataStore(
    private val tokenDao: TokenDao
) {

    fun setActive(token: Token) = tokenDao.setActive(TokenEntity(token = token.token))
}