package hu.mito.androidtest.data.mapper

import hu.mito.androidtest.api.mapper.Mapper
import hu.mito.androidtest.database.entity.TodoItemEntity
import hu.mito.androidtest.domain.model.TodoItem

object ItemEntityMapper : Mapper<TodoItemEntity, TodoItem>() {

    override fun mapFrom(from: TodoItemEntity): TodoItem {
        return TodoItem(
            from.id,
            from.name,
            from.quantity
        )
    }

    override fun mapTo(from: TodoItem): TodoItemEntity {
        return TodoItemEntity(
            from.id,
            from.name,
            from.quantity
        )
    }
}