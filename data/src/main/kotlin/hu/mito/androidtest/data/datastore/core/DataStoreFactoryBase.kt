package hu.mito.androidtest.data.datastore.core

import android.content.Context
import hu.mito.androidtest.domain.datastore.factory.DataStoreFactory

abstract class DataStoreFactoryBase<T>(context: Context) : DataStoreFactory<T>(
        NetworkManagerImpl(context)
)