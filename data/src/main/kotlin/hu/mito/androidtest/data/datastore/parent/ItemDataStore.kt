package hu.mito.androidtest.data.datastore.parent

import hu.mito.androidtest.domain.model.TodoItem
import io.reactivex.Flowable

interface ItemDataStore {

    fun getAll() : Flowable<List<TodoItem>>
}