package hu.mito.androidtest.data.mapper

import hu.mito.androidtest.api.mapper.Mapper
import hu.mito.androidtest.domain.model.TodoItem
import hu.mito.androidtest.network.model.item.ItemResponse

object ItemResponseMapper : Mapper<ItemResponse, TodoItem>() {

    override fun mapFrom(from: ItemResponse): TodoItem {
        return TodoItem(
            id = from.id,
            name = from.name,
            quantity = from.quantity
        )
    }
}