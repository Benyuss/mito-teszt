package hu.mito.androidtest.data.repository

import hu.mito.androidtest.data.datastore.cloud.TokenCloudDataStore
import hu.mito.androidtest.data.datastore.local.TokenLocalDataStore
import hu.mito.androidtest.domain.model.Token
import hu.mito.androidtest.domain.repository.TokenRepository
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers

class TokenRepositoryImpl(
    private val localDataStore: TokenLocalDataStore,
    private val cloudDataStore: TokenCloudDataStore
) : TokenRepository {

    override fun login(username: String, password: String): Completable {
        return cloudDataStore.getActive(username, password)
            .observeOn(Schedulers.io())
            .flatMapCompletable {
                localDataStore.setActive(Token(it.token))
                    .subscribeOn(Schedulers.io())
            }
    }
}