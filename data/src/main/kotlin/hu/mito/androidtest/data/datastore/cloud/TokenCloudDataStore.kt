package hu.mito.androidtest.data.datastore.cloud

import hu.mito.androidtest.network.controller.LoginController
import hu.mito.androidtest.network.model.login.LoginRequest

class TokenCloudDataStore(
    private val loginController: LoginController
) {

    fun getActive(username: String, password: String) =
        loginController.login(LoginRequest(username, password))
}