package hu.mito.androidtest.data.datastore.local

import hu.mito.androidtest.database.dao.TodoItemDao
import hu.mito.androidtest.database.entity.TodoItemEntity

class TodoItemLocalDataStore(
    private val todoItemDao: TodoItemDao
) {

    fun getAll() = todoItemDao.getAll()

    fun addItem(item: TodoItemEntity) = todoItemDao.addItem(item)

    fun addItems(items: List<TodoItemEntity>) = todoItemDao.addItems(items)
}
