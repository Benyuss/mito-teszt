package hu.mito.androidtest.domain.repository

import io.reactivex.Completable

interface TokenRepository {

    fun login(username: String, password: String) : Completable
}