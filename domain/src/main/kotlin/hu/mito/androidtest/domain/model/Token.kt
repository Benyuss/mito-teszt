package hu.mito.androidtest.domain.model

data class Token(
    var token: String
)