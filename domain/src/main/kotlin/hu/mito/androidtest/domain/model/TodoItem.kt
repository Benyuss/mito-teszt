package hu.mito.androidtest.domain.model

data class TodoItem(
    var id: Long,
    var name: String,
    var quantity: Int
)