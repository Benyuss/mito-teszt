package hu.mito.androidtest.domain.interactor

import hu.mito.androidtest.domain.model.TodoItem
import hu.mito.androidtest.domain.repository.TodoItemRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

class TodoItemInteractor(
    private val repository: TodoItemRepository
) {

    fun getItems(): Flowable<List<TodoItem>> = repository.getAll()
        .debounce(300, TimeUnit.MILLISECONDS)

    fun addItem(name: String, quantity: Int): Completable {
        return repository.addItem(name, quantity)
    }
}