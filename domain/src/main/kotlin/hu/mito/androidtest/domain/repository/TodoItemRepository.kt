package hu.mito.androidtest.domain.repository

import hu.mito.androidtest.domain.model.TodoItem
import io.reactivex.Completable
import io.reactivex.Flowable

interface TodoItemRepository {

    fun getAll(): Flowable<List<TodoItem>>

    fun addItem(name: String, quantity: Int): Completable
}