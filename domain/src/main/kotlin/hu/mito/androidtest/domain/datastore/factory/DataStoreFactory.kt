package hu.mito.androidtest.domain.datastore.factory

import hu.mito.androidtest.api.datastore.DataStore
import hu.mito.androidtest.api.datastore.NetworkManager
import hu.mito.androidtest.domain.exception.IllegalDataStoreStateException

abstract class DataStoreFactory<T>(private var networkManager: NetworkManager) {

    fun getDataStore(): DataStore<T> {
        when {
            networkManager.isConnectionAvailable() -> return createCloudDataStore()
            else -> throw IllegalDataStoreStateException()
        }
    }

    abstract fun createCloudDataStore(): DataStore<T>
}