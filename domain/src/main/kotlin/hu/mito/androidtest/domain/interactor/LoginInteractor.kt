package hu.mito.androidtest.domain.interactor

import hu.mito.androidtest.domain.repository.TokenRepository
import io.reactivex.Completable

class LoginInteractor(
    private val tokenRepository: TokenRepository
) {

    fun login(username: String, password: String): Completable {
        return tokenRepository.login(username, password)
    }
}