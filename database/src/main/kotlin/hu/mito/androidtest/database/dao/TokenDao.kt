package hu.mito.androidtest.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hu.mito.androidtest.database.entity.TokenEntity
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface TokenDao {

    @Query("SELECT * FROM token")
    fun getActive(): Maybe<List<TokenEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setActive(token: TokenEntity): Completable
}