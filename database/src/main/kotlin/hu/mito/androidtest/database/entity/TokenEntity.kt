package hu.mito.androidtest.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "token")
data class TokenEntity @JvmOverloads constructor(
    @PrimaryKey var id: Long = 1,
    var token: String
)