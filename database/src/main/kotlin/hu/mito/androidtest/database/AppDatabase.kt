package hu.mito.androidtest.database

import androidx.room.Database
import androidx.room.RoomDatabase
import hu.mito.androidtest.database.dao.TodoItemDao
import hu.mito.androidtest.database.dao.TokenDao
import hu.mito.androidtest.database.entity.TodoItemEntity
import hu.mito.androidtest.database.entity.TokenEntity

@Database(
    entities = [TokenEntity::class, TodoItemEntity::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun tokenDao(): TokenDao
    abstract fun todoItemDao(): TodoItemDao
}