package hu.mito.androidtest.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hu.mito.androidtest.database.entity.TodoItemEntity
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface TodoItemDao {

    @Query("SELECT * FROM todoItems")
    fun getAll(): Flowable<List<TodoItemEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addItem(entity: TodoItemEntity) : Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addItems(entity: List<TodoItemEntity>) : Completable
}