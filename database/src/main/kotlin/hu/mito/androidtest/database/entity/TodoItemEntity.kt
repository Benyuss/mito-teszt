package hu.mito.androidtest.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "todoItems")
data class TodoItemEntity constructor(
    @PrimaryKey var id: Long,
    var name: String,
    var quantity: Int
)