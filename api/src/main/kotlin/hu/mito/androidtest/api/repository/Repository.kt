package hu.mito.androidtest.api.repository

import io.reactivex.Observable

/**
 * TODO: Add a class header comment!
 */
interface Repository<T> {

    fun getOne(): Observable<T>
}