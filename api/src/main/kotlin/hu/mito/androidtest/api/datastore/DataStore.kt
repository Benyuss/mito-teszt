package hu.mito.androidtest.api.datastore

import io.reactivex.Observable

interface DataStore<T> {

    fun getOne(): Observable<T>
}