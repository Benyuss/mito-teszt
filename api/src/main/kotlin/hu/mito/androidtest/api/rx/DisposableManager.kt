package hu.mito.androidtest.api.rx

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class DisposableManager {

    private var compositeDisposable: CompositeDisposable? = null
        get() {
            if (field == null || field!!.isDisposed) {
                compositeDisposable = CompositeDisposable()
            }
            return field
        }

    fun add(disposable: Disposable) {
        compositeDisposable?.add(disposable)
    }

    fun dispose() {
        compositeDisposable?.dispose()
    }
}