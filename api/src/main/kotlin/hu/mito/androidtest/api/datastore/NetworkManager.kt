package hu.mito.androidtest.api.datastore

interface NetworkManager {
    fun isConnectionAvailable(): Boolean
}