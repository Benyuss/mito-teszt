package hu.mito.androidtest.api.presenter

interface Presenter {

    fun onStart()
    fun onStop()
}