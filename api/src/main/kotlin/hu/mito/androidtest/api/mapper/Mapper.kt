package hu.mito.androidtest.api.mapper

/**
 * TODO: Add a class header comment!
 */
abstract class Mapper<E, T> {

    abstract fun mapFrom(from: E): T

    open fun mapTo(from: T): E {
        throw NotImplementedError("Optionally implemented method called.")
    }

    fun mapFromList(from: List<E>): List<T> {
        return from.map { response -> mapFrom(response) }
    }

    fun mapToList(from: List<T>) : List<E> {
        return from.map { response -> mapTo(response) }
    }
}