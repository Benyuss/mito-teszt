package hu.mito.androidtest.api.datastore.subtypes

import hu.mito.androidtest.api.datastore.DataStore

interface CloudDataStore<T> : DataStore<T>