package hu.mito.androidtest.network.endpoint

import hu.mito.androidtest.network.model.login.LoginRequest
import hu.mito.androidtest.network.model.login.TokenResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi {

    @POST("login")
    fun login(@Body request: LoginRequest) : Single<TokenResponse>
}