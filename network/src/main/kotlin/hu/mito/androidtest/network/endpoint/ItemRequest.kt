package hu.mito.androidtest.network.endpoint

data class ItemRequest(
    var name: String,
    var quantity: Int
)