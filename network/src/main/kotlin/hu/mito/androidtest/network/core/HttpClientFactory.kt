package hu.mito.androidtest.network.core

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor

class HttpClientFactory(
    private var token: String? = null
) {

    // todo token management

    val httpClient: OkHttpClient by lazy { makeHttpClient() }
    val authorizedHttpClient by lazy { makeAuthorizedHttpClient() }

    private fun makeHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()

        setDebugModeInterceptor(client)

        return client.build()
    }

    private fun makeAuthorizedHttpClient(): OkHttpClient {

        val client = OkHttpClient.Builder()
            .addInterceptor { chain -> addAuthorizationHeaderInterceptor(chain, token!!) }

        setDebugModeInterceptor(client)

        return client.build()
    }

    private fun setDebugModeInterceptor(client: OkHttpClient.Builder) {
        val interceptor = HttpLoggingInterceptor()

        interceptor.level = HttpLoggingInterceptor.Level.BODY
        client.addInterceptor(interceptor)
    }

    private fun addAuthorizationHeaderInterceptor(
        chain: Interceptor.Chain,
        token: String
    ): Response {
        val originalRequest = chain.request()
        val builder = originalRequest.newBuilder().header("Authorization", "Bearer $token")
        val newRequest = builder.build()
        return chain.proceed(newRequest)
    }
}