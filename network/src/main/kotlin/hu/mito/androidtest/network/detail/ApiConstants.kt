package hu.mito.androidtest.network.detail

interface ApiConstants {

    companion object {
        const val BASE_URL = "https://dev.mito.hu/mobile-test"
    }
}