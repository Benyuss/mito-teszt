package hu.mito.androidtest.network.endpoint

import hu.mito.androidtest.network.model.item.ItemResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ItemApi {

    @GET("items")
    fun getItems(): Single<List<ItemResponse>>

    @POST("item")
    fun addItem(@Body request: ItemRequest): Completable
}