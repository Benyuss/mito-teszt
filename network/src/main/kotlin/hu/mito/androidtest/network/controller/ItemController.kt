package hu.mito.androidtest.network.controller

import android.annotation.SuppressLint
import hu.mito.androidtest.database.dao.TokenDao
import hu.mito.androidtest.database.entity.TokenEntity
import hu.mito.androidtest.domain.model.Token
import hu.mito.androidtest.network.core.ApiClient
import hu.mito.androidtest.network.core.HttpClientFactory
import hu.mito.androidtest.network.endpoint.ItemApi
import hu.mito.androidtest.network.endpoint.ItemRequest
import hu.mito.androidtest.network.model.item.ItemResponse
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class ItemController(private val tokenDao: TokenDao) {

    fun getItems(): Single<List<ItemResponse>> {
        return getToken()
            .flatMapSingle { token ->
                val api = ApiClient.getClient(
                    HttpClientFactory(token.token).authorizedHttpClient
                ).create(ItemApi::class.java)

                api.getItems()
                    .subscribeOn(Schedulers.io())
            }
    }

    fun addItem(request: ItemRequest): Completable {
        return getToken().flatMapCompletable { token ->
            val api = ApiClient.getClient(
                HttpClientFactory(token.token).authorizedHttpClient
            ).create(ItemApi::class.java)

            api.addItem(request)
                .subscribeOn(Schedulers.io())
        }
    }

    @SuppressLint("CheckResult")
    private fun getToken(): Maybe<TokenEntity> {
        return tokenDao.getActive()
            .map { it.first() }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }
}