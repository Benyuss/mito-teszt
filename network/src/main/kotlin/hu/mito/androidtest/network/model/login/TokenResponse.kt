package hu.mito.androidtest.network.model.login

data class TokenResponse(
    var token: String
)