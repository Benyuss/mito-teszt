package hu.mito.androidtest.network.model.login

data class LoginRequest(
    var username : String,
    var password : String
)