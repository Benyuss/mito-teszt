package hu.mito.androidtest.network.model.item

data class ItemResponse(
    var id: Long,
    var name: String,
    var quantity: Int
)