package hu.mito.androidtest.network.controller

import hu.mito.androidtest.network.core.ApiClient
import hu.mito.androidtest.network.core.HttpClientFactory
import hu.mito.androidtest.network.endpoint.LoginApi
import hu.mito.androidtest.network.model.login.LoginRequest
import hu.mito.androidtest.network.model.login.TokenResponse
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class LoginController {

    fun login(request: LoginRequest): Single<TokenResponse> {
        val api = ApiClient.getClient(HttpClientFactory().httpClient).create(LoginApi::class.java)

        return api.login(request)
            .subscribeOn(Schedulers.io())
    }
}